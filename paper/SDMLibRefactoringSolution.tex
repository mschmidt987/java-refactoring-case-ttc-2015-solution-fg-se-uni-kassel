
\documentclass[submission,copyright,creativecommons]{eptcs}
\providecommand{\event}{TTC 2015} 


\usepackage{graphicx,float,enumerate}
\usepackage{listings}
\usepackage{color}
\usepackage{wrapfig}


\graphicspath{{images/}}

\newcommand{\code}{\texttt}
\newcommand{\p}[1]{\textsf{#1}}


\setlength{\textwidth}{16.5cm}
\setlength{\oddsidemargin}{0cm}
\setlength{\evensidemargin}{0cm}
\setlength{\topmargin}{0cm}

\begin{document}

\definecolor{lightgray}{rgb}{.9,.9,.9}
\definecolor{darkgray}{rgb}{.4,.4,.4}
\definecolor{purple}{rgb}{0.5, 0, 0.33}
\definecolor{commentgreen}{rgb}{0.25,0.5,0.37} 
\definecolor{stringblue}{rgb}{0.16, 0, 1}
\definecolor{annotationgrey}{rgb}{0.39,0.39,0.39}


\lstdefinelanguage{Java}{
  keywords={typeof, try, long, void, new, true, false, catch, function, return,
  null, catch, switch, var, if, in, while, do, else, case, break, private, 
  protected,
  final, static, public, class, implements, extends, boolean, throws, throw,
  this, int, float, double}, 
  keywordstyle=\color{purple}\bfseries, ndkeywords={export}, 
  ndkeywordstyle=\color{darkgray}\bfseries,
  identifierstyle=\color{black},
  sensitive=true,
  comment=[l]{//},
  morecomment=[s]{/*}{*/},
  commentstyle=\color{commentgreen}\ttfamily,
  stringstyle=\color{stringblue}\ttfamily,
  morestring=[b]',
  morestring=[b]", 
  moredelim=[il][\textcolor{annotationgrey}]{§§},
  moredelim=[is][\textcolor{annotationgrey}]{\%\%}{\%\%}
}


\pagestyle{headings}

\title{The SDMLib solution to the Java Refactoring case for TTC2015} 
\author{Olaf Gunkel, Matthias Schmidt, Albert Z{\"u}ndorf
\institute{
Kassel University, Software Engineering Research Group,\\
Wilhelmsh{\"o}her Allee 73, 34121 Kassel, Germany\\
\email{olaf.gunkel|matthias.schmidt|zuendorf@cs.uni-kassel.de}
}}

\def\authorrunning{ Albert Z{\"u}ndorf}
\def\titlerunning{The SDMLib solution to the Java Refactoring case for TTC2015}

\maketitle
\begin{center}
{\scriptsize The Solution is hosted under https://bitbucket.org/mschmidt987/java-refactoring-case-ttc-2015-solution-fg-se-uni-kassel}
\end{center}
\begin{abstract}

This paper describes the SDMLib solution to the Java Refactoring case for TTC2015 
\cite{ttc2015-refactoring}. SDMLib provides a mechanism for generating an abstraction model of a provided java program. In addition, SDMLib provides code generation that transforms the whole model or parts of it into java code. Thus, for the Java Refactoring case we just added a \textit{Refactorer} that reads a java project and transforms the program graph according to the intended refactorings. These transformations are collected and applied to the source code by the SDMLib generator afterwards.

\end{abstract}

\section{Introduction}
\label{sec:intro}
Two of our studentical assistants found this case very interesting, because they plan to realize a related case in their master thesis. Their idea is to find bad smells and other structures that should be replaced by a design pattern implementation. After the detection of such places, the replacement should be applied by an automatic refactoring. The implementation of the TTC 2015 refactoring case gave them the chance to have a look on implementing refactorings and estimate the complexity of such code replacement operations.\\
Furthermore, our team gives a lecture in Graph Engineering at the University of Kassel in which we teach master grade students about the theoretical definition of graphs and practical approaches of graph matching and transformation operations. In addition, we teach them to implement a graph matching algorithm to perform transformations on the previously implemented generic graph. So we are familiar with several graph transformation techniques and interested in tasks that can be solved with them. \\
In previous work we already addressed the problem of parsing and generating java source code. To solve this, we added some features to our tool SDMLib. It is able to represent parsed code into a class model, that holds enough information to generate updated code afterwards (without changing the present code where it is not needed). We expected that to be a benefit for us when solving this case.\\
To address the Java Refactoring case, we used the introduced parser of SDMLib to create the program graph before the refactoring. Then we have built a new component to realise the refactorings in the graph. This component uses property change mechanisms to record the changes of the program graph and refactors the source code by calling the SDMLib  generator afterwards.

\section{SDMLib support for source code abstraction and generation}
\label{sec:features}
Transforming java source code into an abstract model is a complex task that can be accomplished by using a powerful parser. To solve this, SDMLib provides a recursive descent parser that is able to analyze java source code files and create an abstract graph model. Using the parser is really easy due to the fact that, as shown in Listing \ref{lst:parsermethodsig}, only the source folder and the package name (where the program lies that should be abstracted) is required.\\
\begin{lstlisting}[language=Java, captionpos=b, 
label=lst:parsermethodsig, escapeinside={\%}{\%},
caption={Signature of the method that calls the parser for java programs}
]
  public void updateFromCode(String srcFolder, String packageName){...}
\end{lstlisting} 
\vspace{\baselineskip}
After parsing the source code, SDMLib provides a model that contains all information required for the refactoring case. The parts of the model, which we use to solve the case, can be seen in Figure \ref{fig:model}. \\

The SDMLib model provides nearly every information that we need for the case.
The only missing information, which is still missing in the solution, is the \textit{access}-assoziation of  class \textit{TMember} as shown in figure 2 of the case description\cite{ttc2015-refactoringpaper}.
Despite the fact that the whole model represents complex program structures, it is comfortable, easy to use and enabled us to fullfill the requirements of the given tasks rapidly. \\
 
\begin{figure}[h!] \centering
 \includegraphics[width=\linewidth]{images/classdiag}
 \caption{Cut of the source code abstraction model}
 \label{fig:model}
\end{figure}

To push our graph changes into the code, SDMLib supports us with its generator, that updates the parsed code. After creating a \textit{ClassModel} by parsing a java project, every included class has its own parser instance, held by the \textit{ClassModel}. The parsers are holding all relevant information about their class. For example, they have symbol tables in which, for every member, information about its position in the sourcecode are stored. By using this position information, its possible to extract, replace and insert parts of the sourcecode. Because of this relation, we can use the symbol table to delete, move or insert members in the source code. Listing \ref{lst:gen} shows how to delete a member from the source file of a class. After replacing entries in the class, we set the boolean field \textit{fileChanged} to \textit{true} and commit the changes to the generating class \textit{CGUtil}. Its \textit{printFile(Parser)} Method writes the changes into the source code files.\newpage

\begin{lstlisting}[language=Java, numbers=left, captionpos=b, 
label=lst:gen, escapeinside={\%}{\%},
caption={How to push changes to the source code with SDMLib}
]
  SymTabEntry memberToDelSTE = clazzParser.getSymTabEntry(delMember); 
 
  clazzParser.replace(memberToDelSTE.getStartPos(), 
  	memberToDelSTE.getEndPos()+1, ""); 
 
  clazzParser.withFileChanged(true); 
 
  CGUtil.printFile(clazzParser);
\end{lstlisting}

\section{Solving the Java refactoring case with SDMLib}
\label{sec:solution}
Our solution covers the three major transformation steps (code to program graph, program graph refactoring and program graph to code) with support for create class-, pull up method-, pull up field- and extract superclass refactoring. \\
%step #1
SDMLib already contains a mechanism to transform code into a program graph. So this part was quite easy to implement. The method \textit{createModelFromSource} in Listing \ref{lst:createmodel} shows how SDMLib can be used to generate a model out of given java source code. Just the path to the project is necessary.\\

\begin{lstlisting}[language=Java, numbers=left, captionpos=b, 
label=lst:createmodel, escapeinside={\%}{\%},
caption={Creating a object model from source code in a given package path}
]
public ClassModel createProgrammGraph(String pathToProject) 
{

   return refactorer.createModelFromSource(pathToProject);

}
\end{lstlisting}
\vspace{\baselineskip}
%step #2
The resulted program graph now must be transformed according to the intended refactoring. Our algorithm is split into two parts here. The first part validates that the refactoring can be applied on the given object structure. For example a pull up method refactoring requires, that all child classes contain the method with the right signature. This requirement is checked for a valid match. The second step executes the graph transformation for the refactoring. Figure \ref{fig:refactoring} shows an example situation for the pull up method refactoring. The method of the first child that should be pulled up gets his class relation changed to the parent. Furthermore we remove the matching methods of all other kids from the graph.\\
%step #3
To complete the last step, we decided to add property change listeners to all relevant members of the object model. These are the methods, classes and attributes, because the refactorings cause changes to them. Our aim was to trace the changes. After the refactoring, the generator of SDMLib applies the traced transformations to the source code. For example our so called \textit{ClazzSuperClassPropertyFileListener} reacts on changes of the inheritance field of a class. If a new superclass is set, this listener 
saves an object of the \textit{ClazzSuperClazzPropertyFileChangeStep} Class in a \textit{Queue}. This queue contains all events with their relevant information. To synchronize model and code, we execute all source code transformations according to the previous done model transformations. In this example, the generator changes the \textit{extends} clauses of the affected classes or generates a new superclass.\\
\begin{figure} \centering
\makebox[\textwidth]{\includegraphics[scale=0.7]{images/refactoring}}
 \caption{Example Graph Transformation for Pull Up Method Refactoring}
 \label{fig:refactoring}
\end{figure}
% summarize
Overall this case was made for us, because SDMLib already had many features to help us creating a program graph and updating the appropriate java source code. Especially the parser and the generator of SDMLib helped to complete these tasks. Furthermore the resulting program graph fullfilled all our needs for the refactorings.

\section{Accomplished testcases}
\label{sec:evaluation}
In Table \ref{tab:execution} all execution times und the result of the given cases are presented. Except of one hidden case, our program succeeds in all tests. The one that fails contains a test where a method of two child classes should not be pulled up, because one of them is accessing a field, that the other one does not have. Our program fails here, because our tool does not analyse the semantic of method bodies. So there are no access edges in our program graph yet. \\
By writing additional tests, we made sure to cover many other cases. The pull up refactoring ensures that the parent class is available. Furthermore we detect whether the pull up method or field is already defined in it, that it has childs and that all childs own the method or field with the right set of parameters. The create superclass refactorer also filters out the corner cases. It ensures that the superclass is not already existing. In addition, the refactoring fails with a response if not all chosen classes have the same superclass.\\
In Table \ref{tab:execution}, all execution times und the result of the given cases are presented. Except of one hidden case, our program succeeds in all tests. The one that fails contains a test where a method of two child classes should not be pulled up, because one of them is accessing a field, that the other one do not have. Our program fails here, because our tool does not analyse the semantic of method bodies. So there are no access edges in our program graph. \\
 By writing additional test cases, we make sure to cover many other cases. The pull up refactoring ensures that the parent class is available. Furthermore we detect whether the pull up method or field is already defined in it, that it has childs and that all childs own the method or field with the right set of parameters. The create superclass refactorer also filters out the corner cases. It ensures that the superclass is not already existing. In addition, the refactoring fails with a response if not all chosen classes have the same superclass.
 
\begin{table}[H]
\centering
\begin{tabular}{|p{3.5cm}|p{1cm}|p{2cm}|}
\hline
 Case & Time(s) & Result \\
\hline
 pub\_ pum3\_ 1 & 0 & SUCCESS\\
 hidden\_ csc3\_ 1a & 0,003 & SUCCESS \\
 hidden\_ csc1\_ 2 & 0,001 & SUCCESS \\
 pub\_ pum1\_ 1\_ paper1 & 0,005 & SUCCESS\\
  hidden\_ csc1\_ 1 & 0,007 & SUCCESS\\
 pub\_ csc1\_ 2 & 0,003 & SUCCESS\\
 hidden\_ pum1\_ 2 & 0,001 & SUCCESS\\
 pub\_ csc1\_ 1 & 0,005 & SUCCESS\\
 hidden\_ pum1\_ 1 & 0,002 & FAILURE\\
 hidden\_ csc2\_ 1 & 0,002 & SUCCESS\\
 pub\_ pum1\_ 2 & 0 & SUCCESS\\
 hidden\_ pum2\_ 2 & 0 & SUCCESS\\
 hidden\_ pum2\_ 1 & 0,002 & SUCCESS\\
 hidden\_ csc3\_ 1 & 0,006 & SUCCESS\\
 pub\_ pum2\_ 1 & 0,001 & SUCCESS\\
 \hline
\end{tabular}
 \caption{Execution time of all given test cases}
\label{tab:execution}
 \end{table}
 \bibliographystyle{abbrv}
\bibliography{sdmlibrefactoringcase}  

\end{document}

 