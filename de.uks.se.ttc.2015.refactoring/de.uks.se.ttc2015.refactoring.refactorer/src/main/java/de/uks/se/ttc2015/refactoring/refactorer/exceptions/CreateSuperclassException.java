package de.uks.se.ttc2015.refactoring.refactorer.exceptions;

public class CreateSuperclassException extends RuntimeException
{

   private static final long serialVersionUID = 7080490909948589451L;

   public CreateSuperclassException ( String message )
   {
      super(message);
   }

}
