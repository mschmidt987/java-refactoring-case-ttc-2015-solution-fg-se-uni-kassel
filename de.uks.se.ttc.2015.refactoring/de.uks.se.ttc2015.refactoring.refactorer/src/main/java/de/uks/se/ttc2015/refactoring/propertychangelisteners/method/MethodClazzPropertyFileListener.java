package de.uks.se.ttc2015.refactoring.propertychangelisteners.method;

import java.beans.PropertyChangeEvent;

import de.uks.se.ttc2015.refactoring.propertychangelisteners.ModelPropertyChangeListener;
import de.uks.se.ttc2015.refactoring.refactorer.RefactorerImpl;
import de.uks.se.ttc2015.refactoring.refactorer.filehandling.refactoringsteps.method.MethodClazzPropertyFileChangeStep;

public class MethodClazzPropertyFileListener extends ModelPropertyChangeListener
{

   public MethodClazzPropertyFileListener ( RefactorerImpl refactorer )
   {
      super(refactorer);

   }

   @Override
   public void propertyChange(PropertyChangeEvent evt)
   {
      this.refactorer.addToFileRefactoringSteps(new MethodClazzPropertyFileChangeStep(evt));

   }

}
