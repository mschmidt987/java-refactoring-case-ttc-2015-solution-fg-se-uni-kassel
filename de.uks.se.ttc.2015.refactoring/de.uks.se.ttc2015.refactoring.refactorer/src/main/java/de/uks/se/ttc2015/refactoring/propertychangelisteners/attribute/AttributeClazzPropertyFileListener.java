package de.uks.se.ttc2015.refactoring.propertychangelisteners.attribute;

import java.beans.PropertyChangeEvent;

import de.uks.se.ttc2015.refactoring.propertychangelisteners.ModelPropertyChangeListener;
import de.uks.se.ttc2015.refactoring.refactorer.RefactorerImpl;
import de.uks.se.ttc2015.refactoring.refactorer.filehandling.refactoringsteps.attribute.AttributeClazzPropertyFileChangeStep;

public class AttributeClazzPropertyFileListener extends ModelPropertyChangeListener
{

   public AttributeClazzPropertyFileListener ( RefactorerImpl refactorer )
   {
      super(refactorer);

   }

   @Override
   public void propertyChange(PropertyChangeEvent evt)
   {

      final AttributeClazzPropertyFileChangeStep step = new AttributeClazzPropertyFileChangeStep(evt);

      this.refactorer.addToFileRefactoringSteps(step);

   }

}
