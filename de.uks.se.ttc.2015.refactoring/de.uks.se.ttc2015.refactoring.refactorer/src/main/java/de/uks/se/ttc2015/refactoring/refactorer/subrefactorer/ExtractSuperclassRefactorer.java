package de.uks.se.ttc2015.refactoring.refactorer.subrefactorer;

import org.sdmlib.models.classes.Attribute;
import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.classes.Method;
import org.sdmlib.models.classes.util.ClazzSet;

import de.uks.se.ttc2015.refactoring.refactorer.RefactorerImpl;

public class ExtractSuperclassRefactorer
{
   private final RefactorerImpl refactorer;

   public ExtractSuperclassRefactorer ( RefactorerImpl refactorer )
   {
      this.refactorer = refactorer;
   }

   public void extractSuperclass(ClassModel clazzModel, ClazzSet childClazzesOfNewSuperclazz, String superclassName)
   {

      //first create a superclass of the childs
      try
      {
         this.refactorer.createSuperclass(clazzModel, childClazzesOfNewSuperclazz, superclassName);
      }
      catch (final Exception e)
      {
         System.err.println(e.getMessage());
      }

      //after creating a superclass the fields must be pulled up where possible
      for (final Attribute attributeOfFirstChild : childClazzesOfNewSuperclazz.get(0).getAttributes())
      {
         try
         {
            this.refactorer.pullUpField(clazzModel, superclassName, attributeOfFirstChild.getName());
         }
         catch (final Exception e)
         {
            System.err.println(e.getMessage());
         }

      }

      //as the last step the methods must be pulled up where possible
      for (final Method methodOfFirstChild : childClazzesOfNewSuperclazz.get(0).getMethods())
      {
         try
         {
            this.refactorer.pullUpMethod(clazzModel, superclassName, methodOfFirstChild.getName(), methodOfFirstChild.getParameter());
         }
         catch (final Exception e)
         {
            System.err.println(e.getMessage());
         }
      }

   }
}
