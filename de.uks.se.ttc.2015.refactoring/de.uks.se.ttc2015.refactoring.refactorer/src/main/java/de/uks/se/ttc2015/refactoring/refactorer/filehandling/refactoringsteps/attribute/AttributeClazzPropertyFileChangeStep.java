package de.uks.se.ttc2015.refactoring.refactorer.filehandling.refactoringsteps.attribute;

import java.beans.PropertyChangeEvent;

import org.sdmlib.CGUtil;
import org.sdmlib.codegen.Parser;
import org.sdmlib.codegen.SymTabEntry;
import org.sdmlib.models.classes.Attribute;
import org.sdmlib.models.classes.Clazz;

import de.uks.se.ttc2015.refactoring.refactorer.filehandling.FileRefactoringStep;

public class AttributeClazzPropertyFileChangeStep implements FileRefactoringStep
{

   private final PropertyChangeEvent event;

   public AttributeClazzPropertyFileChangeStep ( PropertyChangeEvent event )
   {
      this.event = event;

   }

   @Override
   public void execute()
   {
      final Attribute attributeWithChangingClazz = (Attribute) this.event.getSource();

      final Clazz oldClazzOfAttribute = (Clazz) this.event.getOldValue();
      final Clazz newClazzOfAttribute = (Clazz) this.event.getNewValue();

      if (oldClazzOfAttribute != null)
      {
         if (newClazzOfAttribute != null)
         {
            moveAttributeToNewClazz(attributeWithChangingClazz, oldClazzOfAttribute, newClazzOfAttribute);

         }

         deleteAttributeFromOldClazz(attributeWithChangingClazz, oldClazzOfAttribute);

      }
      else
      {
         if (newClazzOfAttribute != null)
         {
            addAttributeToNewClazz(attributeWithChangingClazz, newClazzOfAttribute);

         }

      }

   }

   private void deleteAttributeFromOldClazz(Attribute attributeWithChangingClazz, Clazz oldClazzOfAttribute)
   {
      final Parser oldClazzParser = oldClazzOfAttribute.getClassModel().getGenerator().getOrCreate(oldClazzOfAttribute).getParser().parse();

      final SymTabEntry attributeSymTabEntry = oldClazzParser.getSymTabEntry(Parser.ATTRIBUTE + ":" + attributeWithChangingClazz.getName());

      oldClazzParser.getFileBody().replace(attributeSymTabEntry.getStartPos(), attributeSymTabEntry.getEndPos() + 1, "");

      oldClazzParser.withFileChanged(true);

      CGUtil.printFile(oldClazzParser);
      oldClazzParser.parse();

      deleteAccessorMethods(attributeWithChangingClazz, oldClazzParser);
   }

   private void deleteAccessorMethods(Attribute attributeWithChangingClazz, Parser oldClazzParser)
   {
      final String attributeNameWithFirstCharUp = attributeWithChangingClazz.getName().substring(0, 1).toUpperCase() + attributeWithChangingClazz.getName().substring(1);

      final String setterName = "set" + attributeNameWithFirstCharUp + "(" + attributeWithChangingClazz.getType().getValue() + ")";
      final SymTabEntry setterSymTableEntry = oldClazzParser.getSymTabEntry(Parser.METHOD + ":" + setterName);
      if (setterSymTableEntry != null)
      {
         deleteMethod(setterSymTableEntry, oldClazzParser);
      }

      final String getterName = "get" + attributeNameWithFirstCharUp + "()";
      final SymTabEntry getterSymTableEntry = oldClazzParser.getSymTabEntry(Parser.METHOD + ":" + getterName);
      if (getterSymTableEntry != null)
      {
         deleteMethod(getterSymTableEntry, oldClazzParser);
      }
   }

   private void deleteMethod(SymTabEntry methodToDeleteSymTabEntry, Parser oldClazzParser)
   {

      oldClazzParser.replace(methodToDeleteSymTabEntry.getStartPos(), methodToDeleteSymTabEntry.getEndPos() + 1, "");

      oldClazzParser.withFileChanged(true);

      CGUtil.printFile(oldClazzParser);

      oldClazzParser.parse();

   }

   private void moveAttributeToNewClazz(Attribute attributeWithChangingClazz, Clazz oldClazzOfAttribute, Clazz newClazzOfAttribute)
   {
      final Parser oldClazzParser = oldClazzOfAttribute.getClassModel().getGenerator().getOrCreate(oldClazzOfAttribute).getParser().parse();

      final SymTabEntry attributeSymTabEntry = oldClazzParser.getSymTabEntry(Parser.ATTRIBUTE + ":" + attributeWithChangingClazz.getName());

      final StringBuilder attributeStringBuilder = new StringBuilder();

      attributeStringBuilder.append(oldClazzParser.getFileBody().substring(attributeSymTabEntry.getStartPos(), attributeSymTabEntry.getEndPos() + 1));

      insertAttributeIntoNewClazz(newClazzOfAttribute, attributeStringBuilder);

      moveAccessorMethods(attributeWithChangingClazz, oldClazzParser, newClazzOfAttribute);

   }

   private void moveAccessorMethods(Attribute attributeWithChangingClazz, Parser oldClazzParser, Clazz newClazzOfAttribute)
   {

      final Parser newClazzParser = newClazzOfAttribute.getClassModel().getGenerator().getOrCreate(newClazzOfAttribute).getParser();

      final String attributeNameWithFirstCharUp = attributeWithChangingClazz.getName().substring(0, 1).toUpperCase() + attributeWithChangingClazz.getName().substring(1);

      final String setterName = "set" + attributeNameWithFirstCharUp + "(" + attributeWithChangingClazz.getType().getValue() + ")";
      moveMethod(setterName, oldClazzParser, newClazzParser);

      final String getterName = "get" + attributeNameWithFirstCharUp + "()";
      moveMethod(getterName, oldClazzParser, newClazzParser);

   }

   private void moveMethod(String methodSignature, Parser oldClazzParser, Parser newClazzParser)
   {
      final String methodToMovesignature = Parser.METHOD + ":" + methodSignature;

      final SymTabEntry methodToMoveSymTabEntry = oldClazzParser.getSymTabEntry(methodToMovesignature);

      if (methodToMoveSymTabEntry != null)
      {
         String methodAsString = oldClazzParser.getFileBody().substring(methodToMoveSymTabEntry.getStartPos(), methodToMoveSymTabEntry.getEndPos() + 1);

         //method with indentation
         methodAsString = "   " + methodAsString + "\n";

         newClazzParser.insert(newClazzParser.indexOf(Parser.CLASS_END), methodAsString);

         newClazzParser.withFileChanged(true);

         CGUtil.printFile(newClazzParser);

         newClazzParser.parse();

      }
   }

   private void addAttributeToNewClazz(Attribute attributeWithChangingClazz, Clazz newClazzOfAttribute)
   {
      final StringBuilder attributeStringBuilder = createAttributeString(attributeWithChangingClazz);

      insertAttributeIntoNewClazz(newClazzOfAttribute, attributeStringBuilder);

   }

   private void insertAttributeIntoNewClazz(Clazz newClazzOfAttribute, StringBuilder attributeStringBuilder)
   {
      final Parser newClazzParser = newClazzOfAttribute.getClassModel().getGenerator().getOrCreate(newClazzOfAttribute).getParser().parse();

      insertAttributeString(newClazzParser, newClazzOfAttribute, attributeStringBuilder);

      newClazzParser.withFileChanged(true);

      CGUtil.printFile(newClazzParser);
      newClazzParser.parse();
   }

   private void insertAttributeString(Parser newClazzParser, Clazz newClazzOfAttribute, StringBuilder attributeStringBuilder)
   {

      if (newClazzOfAttribute.getAttributes().size() > 1)
      {
         attributeStringBuilder.append("\n   ");
      }
      else
      {
         attributeStringBuilder.insert(0, "\n   ");
         attributeStringBuilder.append("\n");
      }

      newClazzParser.insert(newClazzParser.indexOf(Parser.CLASS_BODY), attributeStringBuilder.toString());
   }

   private StringBuilder createAttributeString(Attribute attributeWithChangingClazz)
   {

      final StringBuilder attributeStringBuilder = new StringBuilder();

      attributeStringBuilder.append(attributeWithChangingClazz.getVisibility());
      attributeStringBuilder.append(" ");
      attributeStringBuilder.append(attributeWithChangingClazz.getType().getValue());
      attributeStringBuilder.append(" ");
      attributeStringBuilder.append(attributeWithChangingClazz.getName());
      if (attributeWithChangingClazz.getInitialization() != null)
      {
         throw new RuntimeException("unresolved problem: Attribute has an initialisation");
      }
      attributeStringBuilder.append(";");

      return attributeStringBuilder;
   }

}
