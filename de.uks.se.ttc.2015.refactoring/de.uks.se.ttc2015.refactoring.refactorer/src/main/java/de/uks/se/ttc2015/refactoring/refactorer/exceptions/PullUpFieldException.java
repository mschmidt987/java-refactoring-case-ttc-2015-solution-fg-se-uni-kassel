package de.uks.se.ttc2015.refactoring.refactorer.exceptions;

public class PullUpFieldException extends RuntimeException
{

   private static final long serialVersionUID = 7496965172754479358L;

   public PullUpFieldException ( String string )
   {
      super(string);
   }
}
