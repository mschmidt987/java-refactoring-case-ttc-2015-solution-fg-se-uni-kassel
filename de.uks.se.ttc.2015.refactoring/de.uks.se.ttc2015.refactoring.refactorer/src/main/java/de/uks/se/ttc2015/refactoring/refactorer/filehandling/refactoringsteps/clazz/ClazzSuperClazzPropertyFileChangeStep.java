package de.uks.se.ttc2015.refactoring.refactorer.filehandling.refactoringsteps.clazz;

import java.beans.PropertyChangeEvent;

import org.sdmlib.CGUtil;
import org.sdmlib.codegen.Parser;
import org.sdmlib.models.classes.Clazz;

import de.uks.se.ttc2015.refactoring.refactorer.filehandling.FileRefactoringStep;

public class ClazzSuperClazzPropertyFileChangeStep implements FileRefactoringStep
{

   private final PropertyChangeEvent event;

   public ClazzSuperClazzPropertyFileChangeStep ( PropertyChangeEvent event )
   {
      this.event = event;

   }

   @Override
   public void execute()
   {
      final Clazz clazzWithChangedSuperClasses = (Clazz) this.event.getSource();

      final Clazz newSuperClazz = (Clazz) this.event.getNewValue();

      changeExtendsClause(clazzWithChangedSuperClasses, newSuperClazz);

   }

   public void changeExtendsClause(Clazz clazzWithChangedSuperClasses, Clazz newSuperClazz)
   {
      final Parser clazzWithChangedSuperClassesparser = clazzWithChangedSuperClasses.getClassModel().getGenerator().getOrCreate(clazzWithChangedSuperClasses).getParser().parse();

      //there are only 2 possible situations: (superclazz was added -> newSuperClazz!=null & oldSuperClazz==null) | (superclazz was removed -> newSuperClazz==null & oldSuperClazz!=null)

      if (newSuperClazz != null)
      {
         addSuperclazzToCode(newSuperClazz, clazzWithChangedSuperClasses, clazzWithChangedSuperClassesparser);
      }

      else
      {
         deleteSuperclazzFromCode(clazzWithChangedSuperClassesparser);
      }

      clazzWithChangedSuperClassesparser.withFileChanged(true);
      CGUtil.printFile(clazzWithChangedSuperClassesparser);
      clazzWithChangedSuperClassesparser.parse();
   }

   private void addSuperclazzToCode(Clazz newSuperClazz, Clazz clazzWithChangedSuperClasses, Parser clazzWithChangedSuperClassesparser)
   {
      if (newSuperClazz.isInterface())
      {
         addImplementsClauseToClazzCode(newSuperClazz, clazzWithChangedSuperClassesparser);

      }

      //superclazz is no interface -> add extends clause: clazzWithChangedSuperClasses extends newSuperClazz
      else
      {
         addExtendClauseToClazz(newSuperClazz, clazzWithChangedSuperClassesparser);
      }
   }

   private void addImplementsClauseToClazzCode(Clazz newSuperClazz, Parser clazzWithChangedSuperClassesparser)
   {

      final int implementsPos = clazzWithChangedSuperClassesparser.indexOf(Parser.IMPLEMENTS);

      // no existing implementsclause -> add:  implements newSuperClazz
      if (implementsPos == -1)
      {
         final String implementsClause = " implements " + newSuperClazz.getName();

         if (clazzWithChangedSuperClassesparser.indexOf(Parser.EXTENDS) != -1)
         {
            //existing extends clause
            clazzWithChangedSuperClassesparser.insert(clazzWithChangedSuperClassesparser.getEndOfExtendsClause() + 1, implementsClause);

         }

         else
         {
            //no extends clause
            clazzWithChangedSuperClassesparser.insert(clazzWithChangedSuperClassesparser.getEndOfClassName() + 1, implementsClause);

         }

      }

      //existing implementsClause: change "implements a,b,c"-> implements "newSuperClazz,a,b,c"
      else
      {
         final int posToInsertClazzName = implementsPos + "implements".length();
         clazzWithChangedSuperClassesparser.insert(posToInsertClazzName, " " + newSuperClazz.getName() + ", ");

      }

   }

   private void deleteSuperclazzFromCode(Parser clazzWithChangedSuperClassesparser)
   {
      final int extendsStart = clazzWithChangedSuperClassesparser.indexOf(Parser.EXTENDS);
      final int extendsEnd = clazzWithChangedSuperClassesparser.getEndOfExtendsClause() + 1;
      clazzWithChangedSuperClassesparser.replace(extendsStart, extendsEnd, "");
   }

   private void addExtendClauseToClazz(Clazz newSuperClazz, Parser clazzWithChangedSuperClassesparser)
   {
      final StringBuilder newExtendsClause = new StringBuilder();

      newExtendsClause.append(" extends ");
      newExtendsClause.append(newSuperClazz.getFullName());

      //there is already an extends clause -> replace the existing with the new
      if (clazzWithChangedSuperClassesparser.indexOf(Parser.EXTENDS) != -1)
      {
         clazzWithChangedSuperClassesparser.replace(clazzWithChangedSuperClassesparser.indexOf(Parser.EXTENDS),
               clazzWithChangedSuperClassesparser.getEndOfExtendsClause() + 1,
               newExtendsClause.toString());

      }

      else
      {
         final int extendsPos = clazzWithChangedSuperClassesparser.getEndOfClassName() + 1;

         clazzWithChangedSuperClassesparser.insert(extendsPos, newExtendsClause.toString());
      }
   }

}
