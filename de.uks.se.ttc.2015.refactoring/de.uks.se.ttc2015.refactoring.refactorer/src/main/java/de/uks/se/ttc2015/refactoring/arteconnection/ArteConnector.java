package de.uks.se.ttc2015.refactoring.arteconnection;

import java.io.File;

import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.classes.Clazz;
import org.sdmlib.models.classes.DataType;
import org.sdmlib.models.classes.Parameter;
import org.sdmlib.models.classes.util.ClazzSet;
import org.sdmlib.models.classes.util.ParameterSet;

import ttc.testdsl.tTCTest.Create_Superclass_Refactoring;
import ttc.testdsl.tTCTest.Java_Class;
import ttc.testdsl.tTCTest.Pull_Up_Refactoring;
import ttc.testsuite.interfaces.TestInterface;
import de.uks.se.ttc2015.refactoring.refactorer.Refactorer;

public class ArteConnector implements TestInterface
{

   private File permanentStoragePath;
   private File tempPath;
   private final Refactorer refactorer;
   private ClassModel classModel;
   private File logPath;

   public ArteConnector ()
   {
      this.refactorer = Refactorer.createRefactorer();

   }

   @Override
   public boolean applyCreateSuperclass(Create_Superclass_Refactoring crcRefactoring)
   {

      System.out.println("Create Superclass Refactoring started");
      try
      {
         //childnames for output
         final StringBuilder childNamesForOutput = new StringBuilder();

         final ClazzSet childClazzesOfNewSuperclazz = new ClazzSet();

         final String superclassName = crcRefactoring.getTarget().getPackage() + "." + crcRefactoring.getTarget().getClass_name();
         System.out.println("New Superclass with Name: " + superclassName);

         for (final Java_Class currentChildjavaClass : crcRefactoring.getChild().getClasses())
         {
            final String fullqualifiedNameOfCurrentChildClass = currentChildjavaClass.getPackage() + "." + currentChildjavaClass.getClass_name();
            childNamesForOutput.append(fullqualifiedNameOfCurrentChildClass + " ");

            //find the Clazzrepresentation in the SDMLib classModel
            final Clazz currentChildClazz = this.classModel.getClazz(fullqualifiedNameOfCurrentChildClass);

            if (currentChildClazz == null)
            {
               throw new RuntimeException("Could not find children: " + currentChildjavaClass.getClass_name());
            }

            childClazzesOfNewSuperclazz.add(currentChildClazz);
         }

         System.out.println("For childClasses: " + childNamesForOutput.toString());

         this.refactorer.createSuperclass(this.classModel, childClazzesOfNewSuperclazz, superclassName);
      }

      catch (final Exception e)
      {
         System.err.println("Exception in apply create Superclass");

         System.err.println(e.getMessage());

         System.err.println("\n\nCreating Superclass was unsucessful");
         return false;
      }

      System.out.println("Creating Superclass was successful");
      return true;
   }

   @Override
   public boolean applyPullUpMethod(Pull_Up_Refactoring pumRefactoring)
   {

      System.out.println("Pullupmethod Refactoring started");

      try
      {
         final String parentClassName = pumRefactoring.getParent().getPackage() + "." + pumRefactoring.getParent().getClass_name();

         final String methodName = pumRefactoring.getMethod().getMethod_name();

         System.out.println("trying to pull up method: " + methodName + " in " + parentClassName);

         //find all parameters
         final ParameterSet parameterSet = new ParameterSet();
         for (final Java_Class javaClass : pumRefactoring.getMethod().getParams())
         {
            final String class_name = javaClass.getClass_name();
            final DataType datatypeOfParam = DataType.getInstance(class_name);
            final Parameter param = new Parameter(datatypeOfParam);
            parameterSet.add(param);
         }

         this.refactorer.pullUpMethod(this.classModel, parentClassName, methodName, parameterSet);
      }
      catch (final Exception e)
      {
         System.err.println("Error in Pullupmethod");
         System.err.println(e.getMessage());
         System.err.println("\n\nPullupmethod Refactoring was unsuccessful");
         return false;
      }
      System.out.println("Pullupmethod Refactoring was successful");
      return true;
   }

   @Override
   public boolean createProgramGraph(String pathToProject)
   {
      System.out.println("started programmgraph creation");

      try
      {

         this.classModel = this.refactorer.createModelFromSource(pathToProject);

      }
      catch (final Exception e)
      {
         System.err.println("Creating programgraph was unsuccessful");
         e.printStackTrace();
         return false;
      }

      System.out.println("Create programmgraph was successful");
      return true;
   }

   @Override
   public String getPluginName()
   {
      return "SDMLib Refactoring Solution";
   }

   @Override
   public void setLogPath(File logPath)
   {
      System.out.println("Logpath: " + logPath.getAbsolutePath());
      this.logPath = logPath;

   }

   @Override
   public void setPermanentStoragePath(File permanentStoragePath)
   {
      System.out.println("PermanentStoragePath: " + permanentStoragePath.getAbsolutePath());
      this.permanentStoragePath = permanentStoragePath;

   }

   @Override
   public void setTmpPath(File tempPath)
   {
      System.out.println("TempPath: " + tempPath.getAbsolutePath());
      this.tempPath = tempPath;

   }

   @Override
   public boolean synchronizeChanges()
   {
      this.refactorer.syncModelAndCode();
      return true;
   }

   @Override
   public void setProgramLocation(String programLocation)
   {
      System.out.println("Programm Location: " + programLocation);
   }

   @Override
   public boolean usesProgramGraph()
   {
      return true;
   }

}
