package de.uks.se.ttc2015.refactoring.propertychangelisteners.clazz;

import java.beans.PropertyChangeEvent;

import de.uks.se.ttc2015.refactoring.propertychangelisteners.ModelPropertyChangeListener;
import de.uks.se.ttc2015.refactoring.refactorer.RefactorerImpl;
import de.uks.se.ttc2015.refactoring.refactorer.filehandling.refactoringsteps.clazz.ClazzSuperClazzPropertyFileChangeStep;

public class ClazzSuperClassPropertyFileListener extends ModelPropertyChangeListener
{

   public ClazzSuperClassPropertyFileListener ( RefactorerImpl refactorer )
   {
      super(refactorer);

   }

   @Override
   public void propertyChange(PropertyChangeEvent evt)
   {

      final ClazzSuperClazzPropertyFileChangeStep fileChangeStep = new ClazzSuperClazzPropertyFileChangeStep(evt);
      this.refactorer.addToFileRefactoringSteps(fileChangeStep);

   }

}
