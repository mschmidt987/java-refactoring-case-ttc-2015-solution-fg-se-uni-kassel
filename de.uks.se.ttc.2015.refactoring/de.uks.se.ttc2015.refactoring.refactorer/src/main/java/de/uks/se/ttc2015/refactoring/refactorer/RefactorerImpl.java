package de.uks.se.ttc2015.refactoring.refactorer;

import java.io.File;
import java.io.FileFilter;
import java.util.LinkedList;

import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.classes.util.ClazzSet;
import org.sdmlib.models.classes.util.ParameterSet;

import de.uks.se.ttc2015.refactoring.propertychangelisteners.ModelPropertyChangeListenerCommander;
import de.uks.se.ttc2015.refactoring.refactorer.filehandling.FileRefactoringStep;
import de.uks.se.ttc2015.refactoring.refactorer.subrefactorer.CreateSuperclassRefactorer;
import de.uks.se.ttc2015.refactoring.refactorer.subrefactorer.ExtractSuperclassRefactorer;
import de.uks.se.ttc2015.refactoring.refactorer.subrefactorer.PullUpFieldRefactorer;
import de.uks.se.ttc2015.refactoring.refactorer.subrefactorer.PullUpMethodRefactorer;

public class RefactorerImpl implements Refactorer
{

   private PullUpMethodRefactorer pullUpMethodRefactorer;
   private CreateSuperclassRefactorer createSuperclassRefactorer;
   private ExtractSuperclassRefactorer extractSuperclassRefactorer;

   private String projectpath;
   private String includePathes;
   private String packagepath;

   private PullUpFieldRefactorer pullUpFieldRefactorer;
   private final LinkedList<FileRefactoringStep> refactoringSteps;

   private ModelPropertyChangeListenerCommander modelListenerCommander;

   public RefactorerImpl ()
   {
      initSubCommanders();
      initSubRefactorer();
      this.refactoringSteps = new LinkedList<>();
   }

   private void initSubCommanders()
   {
      setModelListenerCommander(new ModelPropertyChangeListenerCommander(this));

   }

   private void initSubRefactorer()
   {
      this.pullUpMethodRefactorer = new PullUpMethodRefactorer(this);
      this.pullUpFieldRefactorer = new PullUpFieldRefactorer(this);
      this.createSuperclassRefactorer = new CreateSuperclassRefactorer(this);
      this.extractSuperclassRefactorer = new ExtractSuperclassRefactorer(this);
   }

   @Override
   public ClassModel createModelFromSource(String projectpath, String includePathes, String packagepath)
   {
      this.projectpath = projectpath;
      this.packagepath = packagepath;
      this.includePathes = includePathes;

      final ClassModel modelFromSource = new ClassModel(packagepath);
      final File projectRoot = new File(projectpath);

      modelFromSource.getGenerator().updateFromCode(includePathes, packagepath, projectRoot);

      getModelListenerCommander().addFilePropertyChangeListeners(modelFromSource);
      System.out.println("loaded model");
      return modelFromSource;
   }

   @Override
   public ClassModel createModelFromSource(String includePathes, String packagepath)
   {

      final File projectroot = new File("");
      final String projectpath = projectroot.getAbsolutePath();

      return createModelFromSource(projectpath, includePathes, packagepath);

   }

   @Override
   public void pullUpMethod(ClassModel modelFromSource, String parentClassName, String methodToPullUpName, ParameterSet parameterSet)
   {
      this.pullUpMethodRefactorer.pullUpMethod(modelFromSource, parentClassName, methodToPullUpName, parameterSet);
   }

   @Override
   public void createSuperclass(ClassModel modelFromSource, ClazzSet allChilds, String superclassName)
   {
      this.createSuperclassRefactorer.createSuperclass(modelFromSource, allChilds, superclassName, this.projectpath, this.includePathes);

   }

   @Override
   public void pullUpField(ClassModel clazzModel, String parentClassName, String fieldName)
   {
      this.pullUpFieldRefactorer.pullUpField(clazzModel, parentClassName, fieldName);

   }

   @Override
   public void extractSuperclass(ClassModel clazzModel, ClazzSet childClazzesOfNewSuperclazz, String superclassName)
   {
      this.extractSuperclassRefactorer.extractSuperclass(clazzModel, childClazzesOfNewSuperclazz, superclassName);
   }

   @Override
   public void syncModelAndCode()
   {
      while (!this.refactoringSteps.isEmpty())
      {
         final FileRefactoringStep currentStep = this.refactoringSteps.poll();
         currentStep.execute();
      }

   }

   public ModelPropertyChangeListenerCommander getModelListenerCommander()
   {
      return this.modelListenerCommander;
   }

   public void setModelListenerCommander(ModelPropertyChangeListenerCommander modelListenerCommander)
   {
      this.modelListenerCommander = modelListenerCommander;
   }

   public void addToFileRefactoringSteps(FileRefactoringStep step)
   {
      this.refactoringSteps.add(step);

   }

   @Override
   public ClassModel createModelFromSource(String pathToProject)
   {
      System.out.println("sourcecodepath: " + pathToProject);

      final StringBuilder packagesStringBuilder = new StringBuilder();

      final StringBuilder srcFolderNamesBuilder = new StringBuilder();

      final File projectRoot = new File(pathToProject);

      //only childFiles which are directorys
      final File[] srcFolders = projectRoot.listFiles((FileFilter) file -> file.isDirectory());

      if ((srcFolders == null) || (srcFolders.length < 1))
      {
         throw new RuntimeException("No Srcfolder");
      }

      for (int srcFolderIndex = 0; srcFolderIndex < srcFolders.length; srcFolderIndex++)
      {
         final File currentSrcFolder = srcFolders[srcFolderIndex];

         if (srcFolderIndex > 0)
         {
            srcFolderNamesBuilder.append("\\s+");
         }
         srcFolderNamesBuilder.append(currentSrcFolder.getName());

         final File[] packages = currentSrcFolder.listFiles();

         if (packages != null)
         {

            for (int packageIndex = 0; packageIndex < packages.length; packageIndex++)
            {
               final File currentpackage = packages[packageIndex];
               if (packageIndex > 0)
               {
                  packagesStringBuilder.append("\\s+");
               }
               packagesStringBuilder.append(currentpackage.getName());
            }
         }
      }

      final ClassModel classModel = this.createModelFromSource(pathToProject, srcFolderNamesBuilder.toString(), packagesStringBuilder.toString());

      return classModel;
   }

}
