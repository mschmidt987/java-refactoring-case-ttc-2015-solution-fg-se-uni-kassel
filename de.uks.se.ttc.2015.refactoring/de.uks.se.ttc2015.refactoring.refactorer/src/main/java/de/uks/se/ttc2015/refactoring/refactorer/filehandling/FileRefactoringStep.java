package de.uks.se.ttc2015.refactoring.refactorer.filehandling;

//FileRefactoringSteps are Strategyobjects for lazy model and file synchronisation
public interface FileRefactoringStep
{

   public abstract void execute();

}
