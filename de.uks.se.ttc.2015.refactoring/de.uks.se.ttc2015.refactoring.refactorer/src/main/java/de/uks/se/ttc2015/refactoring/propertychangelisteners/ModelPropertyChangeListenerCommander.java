package de.uks.se.ttc2015.refactoring.propertychangelisteners;

import org.sdmlib.models.classes.Attribute;
import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.classes.Clazz;
import org.sdmlib.models.classes.Method;

import de.uks.se.ttc2015.refactoring.propertychangelisteners.attribute.AttributeClazzPropertyFileListener;
import de.uks.se.ttc2015.refactoring.propertychangelisteners.clazz.ClazzSuperClassPropertyFileListener;
import de.uks.se.ttc2015.refactoring.propertychangelisteners.method.MethodClazzPropertyFileListener;
import de.uks.se.ttc2015.refactoring.refactorer.RefactorerImpl;

public class ModelPropertyChangeListenerCommander
{

   private MethodClazzPropertyFileListener methodClazzPropertyFileListener;
   private ClazzSuperClassPropertyFileListener clazzSuperClassPropertyFileListener;

   private AttributeClazzPropertyFileListener attributeClazzPropertyFileListener;
   private final RefactorerImpl refactorer;

   public ModelPropertyChangeListenerCommander ( RefactorerImpl refactorer )
   {

      this.refactorer = refactorer;
      initListeners();
   }

   private void initListeners()
   {
      initClazzListeners();
      initMethodListeners();
      initAttributeListeners();

   }

   private void initAttributeListeners()
   {
      this.attributeClazzPropertyFileListener = new AttributeClazzPropertyFileListener(this.refactorer);

   }

   private void initMethodListeners()
   {
      this.methodClazzPropertyFileListener = new MethodClazzPropertyFileListener(this.refactorer);

   }

   private void initClazzListeners()
   {
      this.clazzSuperClassPropertyFileListener = new ClazzSuperClassPropertyFileListener(this.refactorer);

   }

   public void addFilePropertyChangeListeners(ClassModel model)
   {

      addMethodListeners(model);
      addClassListeners(model);
      addAttributeListeners(model);

   }

   private void addAttributeListeners(ClassModel model)
   {
      for (final Attribute currentAttribute : model.getClasses().getAttributes())
      {
         currentAttribute.getPropertyChangeSupport().addPropertyChangeListener(Attribute.PROPERTY_CLAZZ, this.attributeClazzPropertyFileListener);
      }

   }

   private void addClassListeners(ClassModel model)
   {
      for (final Clazz currentClazz : model.getClasses())
      {
         addListenersToClazz(currentClazz);

      }

   }

   public void addListenersToClazz(Clazz currentClazz)
   {
      currentClazz.getPropertyChangeSupport().addPropertyChangeListener(Clazz.PROPERTY_SUPERCLAZZES, this.clazzSuperClassPropertyFileListener);

   }

   public void addMethodListeners(ClassModel model)
   {

      for (final Method currentMethod : model.getClasses().getMethods())
      {
         currentMethod.getPropertyChangeSupport().addPropertyChangeListener(Method.PROPERTY_CLAZZ, this.methodClazzPropertyFileListener);

      }

   }

}
