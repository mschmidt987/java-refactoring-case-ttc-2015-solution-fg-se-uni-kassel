package de.uks.se.ttc2015.refactoring.refactorer.subrefactorer;

import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.classes.Clazz;
import org.sdmlib.models.classes.Method;
import org.sdmlib.models.classes.Parameter;
import org.sdmlib.models.classes.util.ClazzSet;
import org.sdmlib.models.classes.util.MethodSet;
import org.sdmlib.models.classes.util.ParameterSet;

import de.uks.se.ttc2015.refactoring.refactorer.RefactorerImpl;
import de.uks.se.ttc2015.refactoring.refactorer.exceptions.PullUpMethodException;

public class PullUpMethodRefactorer
{

   private final RefactorerImpl refactorer;
   private MethodSet methods;

   public PullUpMethodRefactorer ( RefactorerImpl refactorer )
   {
      this.refactorer = refactorer;

   }

   public void pullUpMethod(ClassModel modelFromSource, String parentClassName, String methodToPullUpName, ParameterSet parameterSet)
   {

      final Clazz parentClazz = modelFromSource.getClazz(parentClassName);

      //first step: validate that the input matches the conditions for a pull up field
      boolean refactoringIsPerformable = checkRequirements(methodToPullUpName, parameterSet, parentClazz);

      //second step: perform the pullup
      if (refactoringIsPerformable)
      {
         performPullUp(parentClazz, methods);
      }

   }

   private boolean checkRequirements(String methodToPullUpName, ParameterSet parameterSet, final Clazz parentClazz)
   {

      if (parentClazz == null)
      {
         //class not in model exception
         throw new PullUpMethodException("Parent Class not found");
      }

      final ClazzSet setWithParent = new ClazzSet();
      setWithParent.add(parentClazz);

      if (matchingMethods(methodToPullUpName, parameterSet, setWithParent).size() > 0)
      {
         //superclass already owns method
         throw new PullUpMethodException("Parent Class already owns the pull-up method");
      }

      if (parentClazz.getKidClazzes() == null)
      {
         //no kids, so no methods to pull up
         throw new PullUpMethodException("Parent Class has no childs");
      }

      final ClazzSet kidClazzes = parentClazz.getKidClazzes();

      if (kidClazzes.size() == 0)
      {
         //no kidclass entries, so no methods to pull up
         throw new PullUpMethodException("Parent Class has no childs");
      }

      methods = matchingMethods(methodToPullUpName, parameterSet, kidClazzes);

      if (methods.size() != kidClazzes.size())
      {
         //not all kids have the searched method (with matching params), so do not pull up
         throw new PullUpMethodException("Methods of child classes do not match the conditions");
      }

      return true;
   }

   private MethodSet matchingMethods(String methodToPullUpName, ParameterSet parameterSet, ClazzSet kidClazzes)
   {
      final MethodSet methods = new MethodSet();

      for (final Method methodOfKidClazz : kidClazzes.getMethods())
      {

         if (methodOfKidClazz.getName().equals(methodToPullUpName))
         {

            if (methodOfKidClazz.getParameter().size() == parameterSet.size())
            {

               final ParameterSet methodParams = methodOfKidClazz.getParameter();

               boolean paramsAreEqual = true;

               for (int paramPos = 0; paramPos < parameterSet.size(); paramPos++)
               {
                  final Parameter parameterOfMethod = methodParams.get(paramPos);
                  final Parameter parameterToMatch = parameterSet.get(paramPos);

                  if (!(parameterOfMethod.getType().equals(parameterToMatch.getType())))
                  {
                     paramsAreEqual = false;
                     break;
                  }
               }

               //method of the current kid has the right name and the right parameters
               if (paramsAreEqual)
               {
                  methods.add(methodOfKidClazz);
               }
            }
         }
      }

      return methods;
   }

   private void performPullUp(Clazz parentClazz, MethodSet methods)
   {
      //pull up -> move the method of the first child to its parent and delete all other methods
      boolean first = true;
      for (final Method kidMethod : methods)
      {
         if (first)
         {
            kidMethod.setClazz(parentClazz);
            first = false;
         }
         else
         {
            kidMethod.removeYou();
         }
      }
   }

}
