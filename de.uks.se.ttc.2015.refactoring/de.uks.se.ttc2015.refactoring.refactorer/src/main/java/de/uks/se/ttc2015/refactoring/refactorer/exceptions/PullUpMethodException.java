package de.uks.se.ttc2015.refactoring.refactorer.exceptions;

public class PullUpMethodException extends RuntimeException
{

   private static final long serialVersionUID = 1328239569515426404L;

   public PullUpMethodException ( String message )
   {
      super(message);
   }

}
