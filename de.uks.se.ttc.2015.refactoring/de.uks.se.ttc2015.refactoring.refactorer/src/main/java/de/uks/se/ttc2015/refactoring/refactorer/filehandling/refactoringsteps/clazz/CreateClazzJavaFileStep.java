package de.uks.se.ttc2015.refactoring.refactorer.filehandling.refactoringsteps.clazz;

import java.io.File;

import org.sdmlib.models.classes.Clazz;
import org.sdmlib.models.classes.logic.GenClass;

import de.uks.se.ttc2015.refactoring.refactorer.filehandling.FileRefactoringStep;

public class CreateClazzJavaFileStep implements FileRefactoringStep
{

   private final Clazz superclass;
   private final String projectpath;
   private final String includePath;

   public CreateClazzJavaFileStep ( Clazz superclass, String projectpath, String includePath )
   {
      this.superclass = superclass;
      this.projectpath = projectpath;
      this.includePath = includePath;

   }

   @Override
   public void execute()
   {
      String absolutpathToGenerate = (new File(this.projectpath + "\\" + this.includePath)).getAbsolutePath();
      absolutpathToGenerate = absolutpathToGenerate.replace("\\", "/");

      final GenClass genClassOfSuperClass = this.superclass.getClassModel().withFeatures(null).getGenerator().getOrCreate(this.superclass);
      genClassOfSuperClass.withFilePath(absolutpathToGenerate);
      genClassOfSuperClass.generate(absolutpathToGenerate, absolutpathToGenerate);

      genClassOfSuperClass.getParser().parse();

   }

}
