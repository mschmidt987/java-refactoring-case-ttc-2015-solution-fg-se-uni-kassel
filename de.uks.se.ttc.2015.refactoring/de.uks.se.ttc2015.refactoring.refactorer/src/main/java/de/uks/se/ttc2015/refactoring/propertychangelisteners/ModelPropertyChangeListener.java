package de.uks.se.ttc2015.refactoring.propertychangelisteners;

import java.beans.PropertyChangeListener;

import de.uks.se.ttc2015.refactoring.refactorer.RefactorerImpl;

public abstract class ModelPropertyChangeListener implements PropertyChangeListener
{

   protected RefactorerImpl refactorer;

   public ModelPropertyChangeListener ( RefactorerImpl refactorer )
   {
      this.refactorer = refactorer;

   }

}
