package de.uks.se.ttc2015.refactoring.refactorer.subrefactorer;

import org.sdmlib.models.classes.Attribute;
import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.classes.Clazz;
import org.sdmlib.models.classes.util.ClazzSet;
import org.sdmlib.models.classes.util.MethodSet;

import de.uks.se.ttc2015.refactoring.refactorer.RefactorerImpl;
import de.uks.se.ttc2015.refactoring.refactorer.exceptions.PullUpFieldException;

public class PullUpFieldRefactorer
{

   private final RefactorerImpl refactorer;
   private Attribute attributeToPullUp;

   public PullUpFieldRefactorer ( RefactorerImpl refactorer )
   {
      this.refactorer = refactorer;
   }

   public void pullUpField(ClassModel clazzModel, String parentClassName, String fieldName)
   {
      final Clazz parentClazz = clazzModel.getClazz(parentClassName);

      //first step: validate that the input matches the conditions for a pull up field
      boolean refactoringIsPerformable = checkRequirements(fieldName, parentClazz);

      //second step: perform the pullup
      if (refactoringIsPerformable)
      {
         pullUpAttribute(parentClazz, attributeToPullUp);
      }

   }

   private boolean checkRequirements(String fieldName, Clazz parentClazz)
   {
      if (parentClazz == null)
      {
         //class not in model exception
         throw new PullUpFieldException("ParentClass not found");
      }

      if (parentClazz.getAttributes().getName().contains(fieldName))
      {
         //parentClazz Already has a field with the same name
         throw new PullUpFieldException("ParentClass already has a field with name " + fieldName);
      }
      if (parentClazz.getKidClazzes().size() == 0)
      {
         //parentClazz has no childrens
         throw new PullUpFieldException("ParentClass has no children");
      }

      attributeToPullUp = getCommonAttributeToPullUp(parentClazz.getKidClazzes(), fieldName);

      if (attributeToPullUp == null)
      {
         //parentClazz has no childrens
         throw new PullUpFieldException("at least one child didn't have the field in the way the others have");
      }

      return true;
   }

   private void pullUpAttribute(Clazz parentClazz, Attribute attributeToPullUp)
   {
      // pull up -> add the attr of first child to the parent, remove the attr of the other kids
      boolean first = true;
      for (final Clazz kidClazz : parentClazz.getKidClazzes())
      {
         if (first)
         {
            attributeToPullUp.setClazz(parentClazz);
            first = false;
         }
         else
         {
            for (final Attribute kidAttribute : kidClazz.getAttributes())
            {
               if (kidAttribute.getName().equals(attributeToPullUp.getName()))
               {
                  kidAttribute.removeYou();

               }
            }

         }
      }

   }

   private Attribute getCommonAttributeToPullUp(ClazzSet kidClazzes, String fieldName)
   {

      Attribute attributeToMatch = null;

      for (final Attribute currentAttrbute : kidClazzes.get(0).getAttributes())
      {
         if (currentAttrbute.getName().equals(fieldName))
         {
            attributeToMatch = currentAttrbute;
            break;
         }
      }
      //first child already didn't has a field with the given name
      if (attributeToMatch == null)
      {
         return null;
      }

      for (int childPos = 0; childPos < kidClazzes.size(); childPos++)
      {
         final Clazz currentChildClazz = kidClazzes.get(childPos);
         if (!childHasSuchAttribute(currentChildClazz, attributeToMatch))
         {
            return null;
         }

      }

      //every child has an attribute like the attribute of the first child
      return attributeToMatch;

   }

   private boolean childHasSuchAttribute(Clazz currentChildClazz, Attribute attributeToMatch)
   {
      for (final Attribute currentAttribute : currentChildClazz.getAttributes())
      {
         if (currentAttribute.getName().equals(attributeToMatch.getName()))
         {
            if (currentAttribute.getType().equals(attributeToMatch.getType()))
            {
               //same name and same type -> equal
               return true;
            }
         }
      }
      return false;
   }
}
