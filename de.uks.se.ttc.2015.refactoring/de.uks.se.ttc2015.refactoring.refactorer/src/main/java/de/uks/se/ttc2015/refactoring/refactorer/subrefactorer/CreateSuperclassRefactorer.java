package de.uks.se.ttc2015.refactoring.refactorer.subrefactorer;

import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.classes.Clazz;
import org.sdmlib.models.classes.util.ClazzSet;

import de.uks.se.ttc2015.refactoring.refactorer.RefactorerImpl;
import de.uks.se.ttc2015.refactoring.refactorer.exceptions.CreateSuperclassException;
import de.uks.se.ttc2015.refactoring.refactorer.filehandling.refactoringsteps.clazz.CreateClazzJavaFileStep;

public class CreateSuperclassRefactorer
{

   private final RefactorerImpl refactorer;
   private Clazz superclass;
   private Clazz existingSuperclass;

   public CreateSuperclassRefactorer ( RefactorerImpl refactorer )
   {
      this.refactorer = refactorer;

   }

   public void createSuperclass(ClassModel modelFromSource, ClazzSet classes, String superclassName, String projectPath, String includePathes)
   {
      final Clazz clazzInModelWithSuperclassName = modelFromSource.getClazz(superclassName);

      //first step: validate that the input matches the conditions for a create superclass
      boolean refactoringIsPerformable = checkRequirements(modelFromSource, classes, superclassName, projectPath, includePathes, clazzInModelWithSuperclassName);

      //second step: perform the refactoring
      if (refactoringIsPerformable)
      {
         //option 1: the childclasses already have a superclass before the refactoring
         if (existingSuperclass != null)
         {
            performSetSuperclassWithExistingSuperClass(classes, superclass, existingSuperclass);
         }
         //option 2: the childclasses don't have a superclass before the refactoring
         else
         {
            performSetSuperclass(classes, superclass);
         }
      }

   }

   private boolean checkRequirements(ClassModel modelFromSource, ClazzSet classes, String superclassName, String projectPath, String includePathes,
         final Clazz clazzInModelWithSuperclassName)
   {
      if (clazzInModelWithSuperclassName != null)
      {
         throw new CreateSuperclassException("Superclass already exists");
      }

      superclass = createSuperClazz(modelFromSource, superclassName, projectPath, includePathes);

      if (classes == null)
      {
         throw new CreateSuperclassException("No classes selected");
      }

      if (classes.size() == 0)
      {
         throw new CreateSuperclassException("No classes selected");
      }

      if (superclass == null)
      {
         throw new CreateSuperclassException("No superclass selected");
      }

      existingSuperclass = classes.get(0).getSuperClass();

      //check that all childs have the same superclass
      boolean allHaveSameSuperclass = true;
      for (final Clazz currentClazz : classes)
      {
         if (!(currentClazz.getSuperClass() == existingSuperclass))
         {
            allHaveSameSuperclass = false;
            break;
         }
      }

      if (!allHaveSameSuperclass)
      {
         throw new CreateSuperclassException("Selected classes have different superclasses");

      }

      return true;
   }

   private Clazz createSuperClazz(ClassModel modelFromSource, String superclassName, String projectpath, String includePath)
   {

      final Clazz superclass = modelFromSource.createClazz(superclassName);
      this.refactorer.getModelListenerCommander().addListenersToClazz(superclass);

      final CreateClazzJavaFileStep createClazzJavaFileStep = new CreateClazzJavaFileStep(superclass, projectpath, includePath);
      this.refactorer.addToFileRefactoringSteps(createClazzJavaFileStep);

      return superclass;
   }

   private void performSetSuperclass(ClazzSet classes, Clazz superclass)
   {
      //apply the superclass to all classes
      for (final Clazz currentClazz : classes)
      {
         if (!(currentClazz.equals(superclass)))
         {
            currentClazz.withSuperClazz(superclass);
         }
      }
   }

   private void performSetSuperclassWithExistingSuperClass(ClazzSet classes, Clazz superclass, Clazz existingSuperclass)
   {
      //delete old superclass from childs, add the new ...
      for (final Clazz currentClazz : classes)
      {
         currentClazz.withoutSuperClazz(existingSuperclass);
         currentClazz.withSuperClazz(superclass);
      }

      //...and let the new superclass extend the old superclass
      superclass.withSuperClazz(existingSuperclass);
   }

}
