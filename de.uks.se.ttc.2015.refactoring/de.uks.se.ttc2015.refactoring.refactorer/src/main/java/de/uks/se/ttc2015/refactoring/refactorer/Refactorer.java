package de.uks.se.ttc2015.refactoring.refactorer;

import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.classes.util.ClazzSet;
import org.sdmlib.models.classes.util.ParameterSet;

//defines the api for the refactorer
public interface Refactorer
{

   public static Refactorer createRefactorer()
   {
      return new RefactorerImpl();
   }

   public ClassModel createModelFromSource(String includePathes, String packagepath);

   public ClassModel createModelFromSource(String projectpath, String includePathes, String packagepath);

   public void pullUpMethod(ClassModel clazzModel, String parentClassName, String methodToPullUpName, ParameterSet parameterSet);

   public void pullUpField(ClassModel clazzModel, String parentClassName, String fieldName);

   public void createSuperclass(ClassModel clazzModel, ClazzSet childClazzesOfNewSuperclazz, String superclassName);

   public void extractSuperclass(ClassModel clazzModel, ClazzSet childClazzesOfNewSuperclazz, String superclassName);

   public void syncModelAndCode();

   public ClassModel createModelFromSource(String pathToProject);

}
