package de.uks.se.ttc2015.refactoring.refactorer.filehandling.refactoringsteps.method;

import java.beans.PropertyChangeEvent;

import org.sdmlib.CGUtil;
import org.sdmlib.codegen.Parser;
import org.sdmlib.codegen.SymTabEntry;
import org.sdmlib.models.classes.Clazz;
import org.sdmlib.models.classes.Method;

import de.uks.se.ttc2015.refactoring.refactorer.filehandling.FileRefactoringStep;

public class MethodClazzPropertyFileChangeStep implements FileRefactoringStep
{

   private final PropertyChangeEvent event;

   private final Method eventMethod;

   private final Clazz oldClazzOfMethod;

   private final Clazz newClazzOfMethod;

   private final Parser oldClazzParser;

   private final String methodSignature;

   public MethodClazzPropertyFileChangeStep ( PropertyChangeEvent event )
   {
      this.event = event;

      this.eventMethod = (Method) this.event.getSource();
      this.oldClazzOfMethod = (Clazz) this.event.getOldValue();
      this.newClazzOfMethod = this.eventMethod.getClazz();
      this.oldClazzParser = this.oldClazzOfMethod.getClassModel().getGenerator().getOrCreate(this.oldClazzOfMethod).getParser().parse();
      this.methodSignature = Parser.METHOD + ":" + this.eventMethod.getSignature(false);

   }

   @Override
   public void execute()
   {

      if (this.newClazzOfMethod != null)
      {
         addMethodToNewClass();
      }

      deleteMethodInOldClass();

   }

   public void addMethodToNewClass()
   {
      final SymTabEntry methodSymTabEntry = this.oldClazzParser.getSymTabEntry(this.methodSignature);

      String methodAsString = this.oldClazzParser.getFileBody().substring(methodSymTabEntry.getStartPos(), methodSymTabEntry.getEndPos() + 1);

      //method with indentation
      methodAsString = "   " + methodAsString + "\n";

      final Parser newClazzParser = this.newClazzOfMethod.getClassModel().getGenerator().getOrCreate(this.newClazzOfMethod).getParser().parse();

      final SymTabEntry symTabEntryInNewClazz = newClazzParser.getSymTabEntry(this.methodSignature);

      if (symTabEntryInNewClazz != null)
      {

         newClazzParser.replace(symTabEntryInNewClazz.getStartPos(), symTabEntryInNewClazz.getEndPos() + 1, methodAsString);
      }
      else
      {
         newClazzParser.insert(newClazzParser.indexOf(Parser.CLASS_END), methodAsString);

      }
      newClazzParser.withFileChanged(true);

      CGUtil.printFile(newClazzParser);

      this.oldClazzParser.parse();

   }

   public void deleteMethodInOldClass()
   {

      final SymTabEntry methodSymTabEntry = this.oldClazzParser.getSymTabEntry(this.methodSignature);

      this.oldClazzParser.replace(methodSymTabEntry.getStartPos(), methodSymTabEntry.getEndPos() + 1, "");

      this.oldClazzParser.withFileChanged(true);

      CGUtil.printFile(this.oldClazzParser);

      this.oldClazzParser.parse();

   }

}
