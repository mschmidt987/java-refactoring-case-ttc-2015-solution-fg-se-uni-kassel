package de.uks.se.ttc2015.refactoring.test.pullupfield.withmethods;

public class ChildWithFieldAndMethodsTwo extends Parent
{

   private int number;

   public int getNumber()
   {
      return number;
   }

   public void setNumber(int number)
   {
      this.number = number;
   }

   private String name;

   public void setName(String name)
   {
      this.name = name;
   }

   public String getName()
   {
      return name;
   }
}
