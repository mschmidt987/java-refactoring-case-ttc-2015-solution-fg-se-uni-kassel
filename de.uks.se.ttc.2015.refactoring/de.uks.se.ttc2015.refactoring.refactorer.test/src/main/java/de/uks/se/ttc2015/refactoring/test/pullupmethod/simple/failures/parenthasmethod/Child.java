package de.uks.se.ttc2015.refactoring.test.pullupmethod.simple.failures.parenthasmethod;

public class Child extends ParentWithMethod
{
   @Override
   public void methodToPullUp(String i, int b)
   {
      System.out.println();
   }
}
