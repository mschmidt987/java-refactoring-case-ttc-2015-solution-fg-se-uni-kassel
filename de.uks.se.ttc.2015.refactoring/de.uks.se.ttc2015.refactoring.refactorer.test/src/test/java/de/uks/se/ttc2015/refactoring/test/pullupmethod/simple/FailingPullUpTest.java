package de.uks.se.ttc2015.refactoring.test.pullupmethod.simple;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.classes.Clazz;
import org.sdmlib.models.classes.DataType;
import org.sdmlib.models.classes.Parameter;
import org.sdmlib.models.classes.util.ParameterSet;

import de.uks.se.ttc2015.refactoring.refactorer.Refactorer;
import de.uks.se.ttc2015.refactoring.refactorer.RefactorerImpl;
import de.uks.se.ttc2015.refactoring.refactorer.exceptions.PullUpMethodException;
import de.uks.se.ttc2015.refactoring.test.RefactoringTestCase;

public class FailingPullUpTest extends RefactoringTestCase
{
   @Test(expected = PullUpMethodException.class)
   public void testFailingMethodPullupInGraphNoParent()
   {
      Refactorer refactorer =  Refactorer.createRefactorer();

      ClassModel modelFromSource = refactorer.createModelFromSource("src/main/java", "de.uks.se.ttc2015.refactoring.test.pullupmethod.simple.failures.noparent");

      assertEquals(0, modelFromSource.getClasses().size());

      ParameterSet expectedParams = new ParameterSet();
      Parameter voidParam = new Parameter("", DataType.VOID);

      expectedParams.add(voidParam);

      try
      {
         refactorer.pullUpMethod(modelFromSource, "Parent", "methodToPullUp", expectedParams);
      }
      catch (PullUpMethodException e)
      {
         assertEquals(e.getMessage(), "Parent Class not found");
         throw e;
      }

   }

   @Test(expected = PullUpMethodException.class)
   public void testFailingMethodPullupInGraphNoChilds()
   {
      Refactorer refactorer = Refactorer.createRefactorer();

      ClassModel modelFromSource = refactorer.createModelFromSource("src/main/java", "de.uks.se.ttc2015.refactoring.test.pullupmethod.simple.failures.nochilds");

      assertEquals(1, modelFromSource.getClasses().size());

      ParameterSet expectedParams = new ParameterSet();
      Parameter voidParam = new Parameter("", DataType.VOID);

      expectedParams.add(voidParam);

      try
      {
         refactorer.pullUpMethod(modelFromSource, "ParentWithoutChilds", "methodToPullUp", expectedParams);
      }
      catch (PullUpMethodException e)
      {
         assertEquals(e.getMessage(), "Parent Class has no childs");
         throw e;
      }

   }

   @Test(expected = PullUpMethodException.class)
   public void testFailingMethodPullupInGraphParentHasMethod()
   {
      RefactorerImpl refactorer = new RefactorerImpl();

      ClassModel modelFromSource = refactorer.createModelFromSource("src/main/java", "de.uks.se.ttc2015.refactoring.test.pullupmethod.simple.failures.parenthasmethod");

      assertEquals(2, modelFromSource.getClasses().size());

      ParameterSet expectedParams = new ParameterSet();
      Parameter stringParam = new Parameter("first", DataType.STRING);
      Parameter intParam = new Parameter("second", DataType.INT);

      expectedParams.add(stringParam);
      expectedParams.add(intParam);

      try
      {
         refactorer.pullUpMethod(modelFromSource, "ParentWithMethod", "methodToPullUp", expectedParams);
      }
      catch (PullUpMethodException e)
      {
         assertEquals(e.getMessage(), "Parent Class already owns the pull-up method");
         throw e;
      }

   }

   @Test(expected = PullUpMethodException.class)
   public void testFailingMethodPullupInGraphParameterMissmatch()
   {
      Refactorer refactorer = Refactorer.createRefactorer();

      ClassModel modelFromSource = refactorer.createModelFromSource("src/main/java", "de.uks.se.ttc2015.refactoring.test.pullupmethod.simple.failures.parametermissmatch");

      assertEquals(3, modelFromSource.getClasses().size());

      Clazz parentClass = modelFromSource.getClazz("Parent");
      Clazz childClazz1 = modelFromSource.getClazz("MatchingChild");
      Clazz childClazz2 = modelFromSource.getClazz("MissMatchingChild");

      assertEquals(1, childClazz1.getMethods().size());
      assertEquals(1, childClazz2.getMethods().size());
      assertEquals(0, parentClass.getMethods().size());

      ParameterSet expectedParams = new ParameterSet();
      Parameter stringParam = new Parameter("first", DataType.STRING);
      Parameter intParam = new Parameter("second", DataType.INT);

      expectedParams.add(stringParam);
      expectedParams.add(intParam);

      try
      {
         refactorer.pullUpMethod(modelFromSource, "Parent", "methodToPullUp", expectedParams);
      }
      catch (PullUpMethodException e)
      {
         assertEquals(e.getMessage(), "Methods of child classes do not match the conditions");
         throw e;
      }

   }

   @Override
   public String givePackageName()
   {
     return "de.uks.se.ttc2015.refactoring.test.pullupmethod.simple.failures.parametermissmatch";
   }

   @Override
   public String giveSrcPath()
   {
      return "src/main/java";
   }

}
