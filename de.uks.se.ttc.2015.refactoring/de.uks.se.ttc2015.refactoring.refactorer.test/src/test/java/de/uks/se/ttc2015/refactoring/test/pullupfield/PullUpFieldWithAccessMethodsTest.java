package de.uks.se.ttc2015.refactoring.test.pullupfield;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.sdmlib.models.classes.Attribute;
import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.classes.Clazz;
import org.sdmlib.models.classes.DataType;
import de.uks.se.ttc2015.refactoring.refactorer.Refactorer;
import de.uks.se.ttc2015.refactoring.test.RefactoringTestCase;

public class PullUpFieldWithAccessMethodsTest extends RefactoringTestCase
{

   @Test
   public void testFieldPullUpWithMethods()
   {
      Refactorer refactorer = Refactorer.createRefactorer();

      ClassModel modelFromSource = refactorer.createModelFromSource("src/main/java", "de.uks.se.ttc2015.refactoring.test.pullupfield.withmethods");

      Clazz parentClass = modelFromSource.getClazz("Parent");
      Clazz childClazz1 = modelFromSource.getClazz("ChildWithFieldAndMethodsOne");
      Clazz childClazz2 = modelFromSource.getClazz("ChildWithFieldAndMethodsTwo");

      assertEquals(2, childClazz1.getAttributes().size());
      assertEquals(2, childClazz2.getAttributes().size());

    

      //pull up private int number;

      refactorer.pullUpField(modelFromSource, "Parent", "number");

      //checks that the field is pulled up
      assertEquals(1, childClazz1.getAttributes().size());
      assertEquals(1, childClazz2.getAttributes().size());

      Attribute fieldOfParentClass = parentClass.getAttributes().get(0);

      assertNotNull(fieldOfParentClass);

      assertEquals("number", fieldOfParentClass.getName());

      assertEquals(DataType.INT, fieldOfParentClass.getType());

      assertEquals(1, parentClass.getAttributes().size());

  
      //pull up private String name;

      refactorer.pullUpField(modelFromSource, "Parent", "name");

      //checks that the field is pulled up
      assertEquals(0, childClazz1.getAttributes().size());
      assertEquals(0, childClazz2.getAttributes().size());

      fieldOfParentClass = parentClass.getAttributes().get(1);

      assertNotNull(fieldOfParentClass);



      assertEquals("name", fieldOfParentClass.getName());

      assertEquals(DataType.STRING, fieldOfParentClass.getType());

      assertEquals(2, parentClass.getAttributes().size());

   }

   @Override
   public String givePackageName()
   {
      return "de.uks.se.ttc2015.refactoring.test.pullupfield.withmethods";
   }

   @Override
   public String giveSrcPath()
   {
      return "src/main/java";
   }

}
