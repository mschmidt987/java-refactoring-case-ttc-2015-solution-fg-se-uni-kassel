package de.uks.se.ttc2015.refactoring.test.createsuperclass.simple;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.classes.Clazz;
import org.sdmlib.models.classes.util.ClazzSet;

import de.uks.se.ttc2015.refactoring.refactorer.Refactorer;
import de.uks.se.ttc2015.refactoring.test.RefactoringTestCase;

public class CreateSuperclassWithInheritanceTest extends RefactoringTestCase
{

   
   @Test
   public void testSuperclassWithExistingInheritance()
   {
      Refactorer refactorer = Refactorer.createRefactorer();

      ClassModel modelFromSource = refactorer.createModelFromSource("src/main/java", "de.uks.se.ttc2015.refactoring.test.createsuperclass.simple.withinheritance");

      Clazz parent = modelFromSource.getClazz("Parent");
      Clazz childOne = modelFromSource.getClazz("ChildOne");
      Clazz childTwo = modelFromSource.getClazz("ChildTwo");

      ClazzSet allChilds = new ClazzSet();
      allChilds.add(childOne);
      allChilds.add(childTwo);

      assertEquals(parent, childOne.getSuperClass());
      assertEquals(parent, childTwo.getSuperClass());

      assertTrue(parent.getKidClazzes().contains(childTwo));
      assertTrue(parent.getKidClazzes().contains(childOne));

      refactorer.createSuperclass(modelFromSource, allChilds, "Superclass");

      Clazz superclass = modelFromSource.getClazz("Superclass");

      assertEquals(superclass, childOne.getSuperClass());
      assertEquals(superclass, childTwo.getSuperClass());

      assertTrue(superclass.getKidClazzes().contains(childTwo));
      assertTrue(superclass.getKidClazzes().contains(childOne));

      assertEquals(superclass.getSuperClass(), parent);

      assertTrue(parent.getKidClazzes().contains(superclass));
   }

   @Override
   public String givePackageName()
   {
      return "de.uks.se.ttc2015.refactoring.test.createsuperclass.simple.withinheritance";
   }

   @Override
   public String giveSrcPath()
   {
      return "src/main/java";
   }
}
