package de.uks.se.ttc2015.refactoring.test.extractsuperclass;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.sdmlib.models.classes.Attribute;
import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.classes.Clazz;
import org.sdmlib.models.classes.Method;

import de.uks.se.ttc2015.refactoring.refactorer.Refactorer;
import de.uks.se.ttc2015.refactoring.test.RefactoringTestCase;

public class ExtractSuperclassTest extends RefactoringTestCase
{

   @Test
   public void testExtractSuperclass()
   {
      Refactorer refactorer = Refactorer.createRefactorer();

      ClassModel modelFromSource = refactorer.createModelFromSource("src/main/java", "de.uks.se.ttc2015.refactoring.test.extractsuperclass");

      assertEquals(2, modelFromSource.getClasses().size());

      Clazz childClazz1 = modelFromSource.getClazz("ChildOne");
      Clazz childClazz2 = modelFromSource.getClazz("ChildTwo");

      assertEquals(1, childClazz1.getMethods().size());
      assertEquals(1, childClazz2.getMethods().size());

      assertEquals(1, childClazz1.getAttributes().size());
      assertEquals(2, childClazz2.getAttributes().size());

      Attribute number = childClazz2.getAttributes().get(0);
      Attribute name = childClazz2.getAttributes().get(1);
   
      refactorer.extractSuperclass(modelFromSource, modelFromSource.getClasses(), "Superclass");
      refactorer.syncModelAndCode();

      Clazz newSuperclass = modelFromSource.getClazz("Superclass");

      assertEquals(3, modelFromSource.getClasses().size());

      assertEquals(0, childClazz1.getMethods().size());
      assertEquals(0, childClazz2.getMethods().size());

      assertEquals(0, childClazz1.getAttributes().size());
      assertEquals(1, childClazz2.getAttributes().size());
      assertTrue(childClazz2.getAttributes().contains(name));

      assertEquals(1, newSuperclass.getMethods().size());

      assertEquals(1, newSuperclass.getAttributes().size());


   }

   @Override
   public String givePackageName()
   {
      return "de.uks.se.ttc2015.refactoring.test.extractsuperclass";
   }

   @Override
   public String giveSrcPath()
   {
      return "src/main/java";
   }

}
