package de.uks.se.ttc2015.refactoring.test.pullupmethod.doublepullup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.classes.Clazz;
import org.sdmlib.models.classes.DataType;
import org.sdmlib.models.classes.Method;
import org.sdmlib.models.classes.util.ParameterSet;

import de.uks.se.ttc2015.refactoring.refactorer.Refactorer;
import de.uks.se.ttc2015.refactoring.test.RefactoringTestCase;

public class DoublePullUpTest extends RefactoringTestCase
{
   @Test
   public void testDoublePullUp()
   {
      Refactorer refactorer = Refactorer.createRefactorer();

      ClassModel modelFromSource = refactorer.createModelFromSource("src/main/java", "de.uks.se.ttc2015.refactoring.test.pullupmethod.doublepullup.situation");

      Clazz childClazz1 = modelFromSource.getClazz("ChildClass1");
      Clazz childClazz2 = modelFromSource.getClazz("ChildClass2");
      Clazz parentClazz1 = modelFromSource.getClazz("ParentClass1");
      Clazz parentClazz2 = modelFromSource.getClazz("ParentClass2");
      Clazz grandParentClazz = modelFromSource.getClazz("GrandParentClass");

      ParameterSet expectedParams = new ParameterSet();

      //first pullup from ChildClass1 and ChildClass2 to ParentClass1
      refactorer.pullUpMethod(modelFromSource, "ParentClass1", "methodToPullUp", expectedParams);

      assertEquals(0, childClazz1.getMethods().size());
      assertEquals(0, childClazz2.getMethods().size());

      Method methodFromParentClass = parentClazz1.getMethods().get(0);

      assertNotNull(methodFromParentClass);


      assertEquals("methodToPullUp", methodFromParentClass.getName());

      assertEquals(DataType.VOID, methodFromParentClass.getReturnType());

      assertEquals(0, methodFromParentClass.getParameter().size());

      Method methodFromParent1 = parentClazz1.getMethods().get(0);

      //second pullup from ParentClass1 and ParentClass2 to GrandParentClass
      refactorer.pullUpMethod(modelFromSource, "GrandParentClass", "methodToPullUp", expectedParams);

      assertEquals(0, parentClazz1.getMethods().size());
      assertEquals(0, parentClazz2.getMethods().size());

      Method methodFromGrandParentClass = grandParentClazz.getMethods().get(0);

      assertNotNull(methodFromGrandParentClass);

      assertEquals("methodToPullUp", methodFromGrandParentClass.getName());

      assertEquals(DataType.VOID, methodFromGrandParentClass.getReturnType());

      assertEquals(0, methodFromGrandParentClass.getParameter().size());
   }

   @Override
   public String givePackageName()
   {
      return "de.uks.se.ttc2015.refactoring.test.pullupmethod.doublepullup.situation";
   }

   @Override
   public String giveSrcPath()
   {
      return "src/main/java"; 
   }

}
