package de.uks.se.ttc2015.refactoring.test.pullupmethod.simple;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.sdmlib.models.classes.ClassModel;
import org.sdmlib.models.classes.Clazz;
import org.sdmlib.models.classes.DataType;
import org.sdmlib.models.classes.Method;
import org.sdmlib.models.classes.Parameter;
import org.sdmlib.models.classes.util.ParameterSet;

import de.uks.se.ttc2015.refactoring.refactorer.Refactorer;
import de.uks.se.ttc2015.refactoring.refactorer.exceptions.PullUpMethodException;
import de.uks.se.ttc2015.refactoring.test.RefactoringTestCase;

public class SimplePullUpTest extends RefactoringTestCase
{



   @Test
   public void testCreateModelFromSource()
   {

      Refactorer refactorer = Refactorer.createRefactorer();

      ClassModel modelFromSource = refactorer.createModelFromSource("src/main/java", "de.uks.se.ttc2015.refactoring.test.pullupmethod.simple.situation");

      assertEquals(3, modelFromSource.getClasses().size());

      Clazz parentClass = modelFromSource.getClazz("SimpleParentClass");

      assertEquals(0, parentClass.getMethods().size());

      Clazz childClazz1 = modelFromSource.getClazz("SimpleChildClass1");
      Clazz childClazz2 = modelFromSource.getClazz("SimpleChildClass2");

      assertEquals(2, parentClass.getKidClazzes().size());

      assertEquals(parentClass, childClazz1.getSuperClass());
      assertEquals(parentClass, childClazz2.getSuperClass());

      assertEquals(1, childClazz1.getMethods().size());
      assertEquals(1, childClazz2.getMethods().size());

      Method methodFromChildClazz1 = childClazz1.getMethods().get(0);

      assertEquals("methodToPullUp", methodFromChildClazz1.getName());

      assertEquals(DataType.VOID, methodFromChildClazz1.getReturnType());

      assertEquals(2, methodFromChildClazz1.getParameter().size());

      Method methodFromChildClazz2 = childClazz2.getMethods().get(0);

      assertEquals("methodToPullUp", methodFromChildClazz2.getName());

      assertEquals(DataType.VOID, methodFromChildClazz2.getReturnType());

      assertEquals(2, methodFromChildClazz2.getParameter().size());

   }

   @Test
   public void testSimpleMethodPullupInGraph()
   {
      Refactorer refactorer = Refactorer.createRefactorer();

      ClassModel modelFromSource = refactorer.createModelFromSource("src/main/java", "de.uks.se.ttc2015.refactoring.test.pullupmethod.simple.situation");

      Clazz parentClass = modelFromSource.getClazz("SimpleParentClass");
      Clazz childClazz1 = modelFromSource.getClazz("SimpleChildClass1");
      Clazz childClazz2 = modelFromSource.getClazz("SimpleChildClass2");

      assertEquals(1, childClazz1.getMethods().size());
      assertEquals(1, childClazz2.getMethods().size());

      ParameterSet expectedParams = new ParameterSet();
      Parameter stringParam = new Parameter("first", DataType.STRING);
      Parameter intParam = new Parameter("second", DataType.INT);

      expectedParams.add(stringParam);
      expectedParams.add(intParam);


      try
      {
         refactorer.pullUpMethod(modelFromSource, "SimpleParentClass", "methodToPullUp", expectedParams);
         refactorer.syncModelAndCode();
      }
      catch (PullUpMethodException e)
      {
         e.printStackTrace();
      }

      assertEquals(0, childClazz1.getMethods().size());
      assertEquals(0, childClazz2.getMethods().size());

      Method methodFromParentClass = parentClass.getMethods().get(0);

      assertNotNull(methodFromParentClass);


      assertEquals("methodToPullUp", methodFromParentClass.getName());

      assertEquals(DataType.VOID, methodFromParentClass.getReturnType());

      assertEquals(2, methodFromParentClass.getParameter().size());

   }

   public void testSimplePullupInFile()
   {
      Refactorer refactorer = Refactorer.createRefactorer();

      ClassModel modelFromSource = refactorer.createModelFromSource("src/main/java", "de.uks.se.ttc2015.refactoring.test.pullupmethod.simple.situation");

      Clazz childClazz1 = modelFromSource.getClazz("SimpleChildClass1");
      Method methodFromChild1 = childClazz1.getMethods().get(0);

      try
      {
         refactorer.pullUpMethod(modelFromSource, "SimpleParentClass", "methodToPullUp", methodFromChild1.getParameter());
      }
      catch (PullUpMethodException e)
      {
         e.printStackTrace();
      }

      fail("Tests are not implemented yet");

   }

   @Override
   public String giveSrcPath()
   {
     return "src/main/java";
   }

   @Override
   public String givePackageName()
   {
      return "de.uks.se.ttc2015.refactoring.test.pullupmethod.simple.situation";
   }

}
