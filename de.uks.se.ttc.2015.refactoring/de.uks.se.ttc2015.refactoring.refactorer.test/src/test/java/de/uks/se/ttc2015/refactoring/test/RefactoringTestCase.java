package de.uks.se.ttc2015.refactoring.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;


public abstract class RefactoringTestCase
{

   private static String srcPath;
   private static String packageName;
   private Map<String, String> fileNameToEntry;
   private String folderName;
   private File folder;




   public  abstract String givePackageName();

   public abstract  String giveSrcPath();

   @Before
   public void saveFiles()
   {
      srcPath = giveSrcPath();
      packageName = givePackageName();
      folderName = srcPath +"/"+ packageName.replace(".", "/");
      folder = new File(folderName);
      File[] filesToSave = folder.listFiles();

      fileNameToEntry = new LinkedHashMap<>();

      for (File currentFile : filesToSave)
      {
         fileNameToEntry.put(currentFile.getName(), parseFileEntry(currentFile));
      }

   }

   private String parseFileEntry(File currentFile)
   {
      StringBuilder fileEntry = new StringBuilder();
      String currentLine = "";
      try
      {
         BufferedReader fileReader = new BufferedReader(new FileReader(currentFile));

         while ((currentLine = fileReader.readLine()) != null)
         {
            fileEntry.append(currentLine);
            fileEntry.append("\n");
         }
         fileReader.close();
      }
      catch (IOException e)
      {

         e.printStackTrace();
      }
      return fileEntry.toString();

   }

   @After
   public void resetFiles()
   {
      for (File subfile : folder.listFiles())
      {
         subfile.delete();
      }
      
      for (String currentFileName : fileNameToEntry.keySet())
      {
         writeFile(currentFileName, fileNameToEntry.get(currentFileName));
      }

   }

   private void writeFile(String currentFileName, String entry)
   {
      File currentFile = new File(folderName+"/"+currentFileName);
      
      currentFile.delete();
      
      FileWriter fileWriter;
      try
      {
         fileWriter = new FileWriter(currentFile,true);
         fileWriter.write(entry);
         
         fileWriter.flush();
         fileWriter.close();
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      
      
   }

}
